@extends('layouts.app', ['title' => 'Karyawan'])

@section('content')
    <h2>Data Karyawan</h2>

    <div class="d-md-flex justify-content-md-end mt-3 mb-3">
        <a href="/karyawan/create" class="btn btn-dark">Input Data</a>
    </div>

    @if (session()->has('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('success') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <div class="row mt-3">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr class="table-light">
                            <th scope="col">#</th>
                            <th scope="col">Nama Karyawan</th>
                            <th scope="col">No. Karyawan</th>
                            <th scope="col">No. Telepon</th>
                            <th scope="col">Jabatan</th>
                            <th scope="col">Divisi</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data_karyawan as $karyawan)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <td>{{ $karyawan->nama_karyawan }}</td>
                                <td>{{ $karyawan->no_karyawan }}</td>
                                <td>{{ $karyawan->no_telp_karyawan }}</td>
                                <td>{{ $karyawan->jabatan_karyawan }}</td>
                                <td>{{ $karyawan->divisi_karyawan }}</td>
                                <td>
                                    <a href="/karyawan/{{ $karyawan->id }}/edit" class="badge bg-warning text-white"><i
                                            class="bi bi-pencil-fill"></i></a>
                                    <form action="/karyawan/{{ $karyawan->id }}" method="POST" class="d-inline">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="badge bg-danger border-0"
                                            onclick="return confirm('Apakah anda yakin?')">
                                            <i class="bi bi-trash-fill"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
